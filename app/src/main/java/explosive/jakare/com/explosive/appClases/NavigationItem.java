package explosive.jakare.com.explosive.appClases;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by andresvasquez on 14/9/16.
 */

public class NavigationItem {

    @SerializedName("id")
    private long id;

    @SerializedName("title")
    private String title;

    @SerializedName("link")
    private String link;

    @SerializedName("isDone")
    private boolean isDone;

    @SerializedName("section")
    private String section;

    @SerializedName("childs")
    public List<NavigationItem> childs;

    public NavigationItem(long id, String title, String link, String section, boolean isDone, List<NavigationItem> childs) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.section=section;
        this.isDone = isDone;
        this.childs = childs;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<NavigationItem> getChilds() {
        return childs;
    }

    public void setChilds(List<NavigationItem> childs) {
        this.childs = childs;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
